<?php

namespace App\Traits;

use GuzzleHttp\Client;


/**
 * Trait TelegramBotTrait
 * @package App\Traits
 */
trait TelegramBotTrait
{
    private $version = "1.0";
    private $apiUrl = "https://api.bitflip.cc/method/";

    public function send($method, $token, $data) {
        $requestData = [
            "version" => $this->version
        ];
        $requestData = array_merge($requestData, $data);

        $headers = [
            'X-API-Token' => $token->token,
            'X-API-Sign' => hash_hmac("sha512", json_encode($requestData), $token->secret),
            'content-type' => 'application/json',
        ];

        try {
            $client = new Client(['headers' => $headers]);
            $response = $client->post($this->apiUrl.$method, ["body" => json_encode($requestData)]);
//            $response = $client->request('GET', "https://ya.ru");
//            dd((string) $response->getBody());
        } catch (RequestException $e) {
            echo $e->getRequest() . "\n";
            if ($e->hasResponse()) {
                echo $e->getResponse() . "\n";
            }
        }

        $response = json_decode((string)  $response->getBody());
        if (is_null($response) || (isset($response[0]) && !is_null($response[0])))
            return false;
        return $response[1];
    }
}
