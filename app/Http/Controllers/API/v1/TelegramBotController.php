<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;
use App\Token;
use Telegram\Bot\Laravel\Facades\Telegram;


class TelegramBotController extends Controller
{
    public function webhook() {
        $telegram = new Api(config("telegram.bot_token"));

        $response = collect($telegram->getWebhookUpdates());

        Log::info("request: ", collect($response)->toArray());


        if (isset($response['callback_query'])) {
            $message = $response['callback_query']['data'];
            $chatId = $response['callback_query']['message']["chat"]["id"];
        }
        else {
            $message = $response['message']['text'];
            $chatId = $response['message']["chat"]["id"];
        }



        if (!empty($message)) {
            if ($message == "/help") {
                $response = '<b>Список доступных команд</b>' . "\n\n";
                $response .= 'Список добавленных токенов' . "\n";
                $response .= '<pre>/tokens</pre>'. "\n";

                $response .= 'Добавление нового токена' . "\n";
                $response .= '<pre>/addtoken {token}:{secret}</pre>'. "\n";

                $response .= 'Удаление добавленного ранее токена' . "\n";
                $response .= '<pre>/deletetoken {token}</pre>'. "\n";

                $telegram->sendMessage([
                    'chat_id' => $chatId,
                    'parse_mode' => 'HTML',
                    'text' => $response,
                ]);

            }
            else if (preg_match('/addtoken \S{8}\-\S{8}\-\S{8}\-\S{8}\-\S{8}:\S{64}$/', $message) === 1) {
                preg_match('/addtoken (\S{8}\-\S{8}\-\S{8}\-\S{8}\-\S{8}):(\S{64})$/', $message, $matches, PREG_OFFSET_CAPTURE, 0);
                $token = $matches[1][0];
                $secret = $matches[2][0];

                try {
                    Token::whereToken($token)->firstOrFail();

                    $response = "<b>Ошибка!</b> Этот токен был зарегистрирован ранее.";
                }
                catch (ModelNotFoundException $exception) {
                    Token::create([
                        "chat_id" => $chatId,
                        "token" => $token,
                        "secret" => $secret
                    ]);

                    $response = "Поздравляю! Ваш токен успешно зарегистрирован!";
                }

                $telegram->sendMessage([
                    'chat_id' => $chatId,
                    'parse_mode' => 'HTML',
                    'text' => $response,
                ]);
            } else if (preg_match('/deletetoken \S{8}\-\S{8}\-\S{8}\-\S{8}\-\S{8}$/', $message) === 1) {
                preg_match('/deletetoken (\S{8}\-\S{8}\-\S{8}\-\S{8}\-\S{8})$/', $message, $matches, PREG_OFFSET_CAPTURE, 0);
                $token = $matches[1][0];

                try {
                    $token = Token::whereToken($token)->whereChatId($chatId)->firstOrFail();
                    $token->delete();

                    $response = "Токен успешно удален";
                }
                catch (ModelNotFoundException $exception) {
                    $response = "<b>Ошибка!</b> Токен не зарегистрирован в системе";
                }

                $telegram->sendMessage([
                    'chat_id' => $chatId,
                    'parse_mode' => 'HTML',
                    'text' => $response,
                ]);
            }
            elseif ($message == "/tokens") {
                $tokens = Token::whereChatId($chatId)->get();

                $response = '<b>Список добавленных токенов</b>' . "\n\n";
                $replyMarkup = [];

                if ($tokens->count() > 0) {
                    $response .= "<i>Нажмите на токен, чтобы удалить его.</i>";

                    $tokenButtons = [];
                    foreach ($tokens as $token) {
                        $tokenButtons[] = [
                            'text' => $token->token, 'callback_data' => '/deletetoken '.$token->token
                        ];
                    }

                    $keyboard = ["inline_keyboard" => [
                        $tokenButtons
                    ]];
                    $replyMarkup = json_encode($keyboard);

                    $telegram->sendMessage([
                        'chat_id' => $chatId,
                        'parse_mode' => 'HTML',
                        'text' => $response,
                        'reply_markup' => $replyMarkup
                    ]);
                }
                else {
                    $response .= "Нет добавленных токенов.";
                    $replyMarkup = json_encode([]);

                    $telegram->sendMessage([
                        'chat_id' => $chatId,
                        'parse_mode' => 'HTML',
                        'text' => $response,
                    ]);
                }
            } else {
                $response = 'Добро пожаловать в бота для получения уведомлений о завершенных транзакциях на бирже BitFlip!' . "\n\n";

                $keyboard = ["inline_keyboard" => [[['text' => 'Список команд', 'callback_data' => '/help']]]];
                $replyMarkup = json_encode($keyboard);

                $telegram->sendMessage([
                    'chat_id' => $chatId,
                    'parse_mode' => 'HTML',
                    'text' => $response,
                    'reply_markup' => $replyMarkup
                ]);


            }
        }

    }

}

