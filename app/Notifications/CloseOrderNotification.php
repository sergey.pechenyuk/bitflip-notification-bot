<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class CloseOrderNotification extends Notification
{
    use Queueable;

    private $chat_id = null;
    private $pair = null;
    private $orders = null;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($chat_id, $pair, $orders)
    {
        $this->chat_id = $chat_id;
        $this->pair = $pair;
        $this->orders = $orders;
    }
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [TelegramChannel::class];
    }

    public function toTelegram($notifiable)
    {
        return TelegramMessage::create()
            ->to($this->chat_id)
            ->content($this->getMessage());
    }

    private function getMessage() {
        list($from, $to) = explode(":", $this->pair);

        $message = "";

        foreach ($this->orders as $order)
            $message .= sprintf(
                "Ваша заявка на %s %f%s на сумму %f%s успешно проведена",
                $order->type == "buy"? "покупку": "продажу",
                $order->open_amount,
                strtoupper($from),
                $order->open_amount * $order->rate,
                strtoupper($to)
            )."\n";

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
