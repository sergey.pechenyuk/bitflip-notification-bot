<?php

namespace App\Console\Commands;

use App\Notifications\CloseOrderNotification;
use App\Order;
use App\Traits\TelegramBotTrait;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Token;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Notification;

class GetUserOrdersCommand extends Command
{
    use TelegramBotTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:get-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Получить информацию о закрытых заказах';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function getPairs() {
        return [
            "usd:eur",
            "usd:rub",
            "btc:usd",
            "btc:eur",
            "btc:rub",
            "ltc:usd",
            "ltc:eur",
            "ltc:rub",
            "eth:usd",
            "eth:eur",
            "eth:rub",
            "xrp:usd",
            "xrp:eur",
            "xrp:rub",
            "dash:usd",
            "dash:eur",
            "dash:rub",
            "doge:usd",
            "doge:eur",
            "doge:rub",
            "bch:usd",
            "bch:eur",
            "bch:rub",
            "flip:usd",
            "flip:eur",
            "flip:rub",
            "r:usd",
            "r:eur",
            "r:rub",
            "rmc:usd",
            "rmc:eur",
            "rmc:rub",
            "btg:usd",
            "btg:eur",
            "btg:rub",
            "xrb:usd",
            "xrb:eur",
            "xrb:rub",
            "trx:usd",
            "trx:eur",
            "trx:rub",
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tokens = Token::with("orders")->where('updated_at', '<=', Carbon::now()->subSeconds(30))->get();

        foreach ($tokens as $token) {
            $timestamp = $token->updated_at->timestamp;
            $orders = [];

            foreach ($this->getPairs() as $pair) {
                $output = $this->send("market.getUserOrders", $token, [
                    "pair" => $pair,
                    "state" => "closed",
                ]);

                if ($output !== [] && $output !== false) {
                    foreach ($output as $order) {
                        try {
                            $token->orders()->where("order_id", $order->id)->firstOrFail();
                        }
                        catch (ModelNotFoundException $exception) {
                            $orders[$pair][] = $order;
                            $newOrder = Order::create([
                                "order_id" => $order->id
                            ]);
                            $token->orders()->save($newOrder);
                        }
                    }
                }
            }

            if ($orders !== []) {
                foreach ($orders as $pair => $order) {
                    Notification::send(null, new CloseOrderNotification($token->chat_id, $pair, $order));
                }
            }

            unset($orders);
        }
    }
}
