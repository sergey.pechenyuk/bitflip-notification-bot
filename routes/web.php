<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('set', function () {
    $re = '/addtoken (\S{8}\-\S{8}\-\S{8}\-\S{8}\-\S{8}):(\S{64})$/';
    $str = 'addtoken 3968433b-e9a4dda4-d9ce31dc-be2b6c58-1de41f13:4405307c2ad15c32e1694a7190cd7eac9b7597b470a3643da411c9aa76e3acc8';

    preg_match($re, $str, $matches, PREG_OFFSET_CAPTURE, 0);

// Print the entire match result
    dd($matches);
});

